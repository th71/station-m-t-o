* Expression des besoins
** Exigences fonctionnelles principales
Guidé par [[http://makerspace56.org/projet-personnel-2019-station-meteo-retro-futuriste][les objectifs]], reformulons ceux-ci en termes d'*exigences
fonctionnelles*.  celles-ci, exprimées bien peu poétiquement, doivent
être cohérentes, non ambiguës et vérifiables afin de permettre par la
suite de tester le résultat projet par rapport aux attentes initiales.

Il s'agit avant tout de *recueillir des données* et de *les
transmettre* pour permettre leur visualisation.
*** Données à recuillir
    Les données à recueillir sont :
    - la température,
    - l'indice d'hygrométrie,
    - la pression atmosphérique,
    - la vitesse du vent,
    - la direction du vent,
    - la pluviométrie,
    - l'état de la couverture nuageuse,
    - l'état de l'ensoleillement.
      
     
    Ces données seront recueillies tant sous forme analogique
    (informations visuelles) que sous forme numérique.

    Les informations visuelles seront dans la mesure du possible
    traitées pour obtenir une valeur numérique.

    On disposera donc de deux source d'informations numériques : des
    informations natives (obtenues par un composant spécialisé) et des
    informations reconstruites (à partir des informations visuelles).
**** Informations visuelles
     Devront être restituées les *photographies* 
     - du *thermomètre*
     - de l'*hygromètre*
     - du *baromètre*
     - du *ciel* en évitant les arbres et autres objets (à l'exception
       des oiseaux bien sûr).
**** Informations numériques
     - Les informations visuelles ci-dessus seront traitées pour obtenir :
       - une valeur pour la *[[http://www.meteofrance.fr/publications/glossaire/154090-temperature][température]]*
       - une valeur pourl'indice d'*[[http://www.meteofrance.fr/publications/glossaire/152198-hauteur-de-precipitation][hygrométrie]]*
       - une valeur pour la *[[http://www.meteofrance.fr/publications/glossaire/153309-pression-atmospherique][pression atmosphérique]]*
       - une valeur pour la *couverture nuageuse*
       - une valeur pour la *luminosité extérieure*
     - En parallèle un composant spécifique sera interrogé pour
       obtenir des valeurs nativement numériques pour
       - la *température*
       - l'*hygrométrie*
       - la *pression atmosphérique*
       - la *luminosité extérieure*
     - des équipements dédiés devront permettre de déterminer :
       - la *[[http://www.meteofrance.fr/publications/glossaire/152198-hauteur-de-precipitation][hauteur de précipitation]]*
       - la *[[http://www.meteofrance.fr/publications/glossaire/154695-vitesse-du-vent][vitesse du vent]]* 
       - la *[[http://www.meteofrance.fr/publications/glossaire/150367-direction-du-vent][direction du vent]]*

*** Précision et fréquences de relevé des données

On récapitule les informations dans le tableau ci-dessous :

 | Type       | Donnée                   | Unité | Valeur min | Valeur Max | Pas | Relevé  |   |
 |------------+--------------------------+-------+------------+------------+-----+---------+---|
 | Analogique | Photographies            | NA    |         NA |         NA |  NA | 1/h     |   |
 |------------+--------------------------+-------+------------+------------+-----+---------+---|
 | Numérique  | Température              | °C    |         -8 |         37 |   1 | 1/h     |   |
 |            | Humidité relative        | %     |          0 |        100 |   1 | 1/h     |   |
 |            | Pression atmosphérique   | hPa   |        900 |       1035 |   5 | 1/h     |   |
 |            | Hauteur de précipitation | mm/h  |          0 |       5000 |   1 | 1/h     |   |
 |            | Vitesse du vent          | km/h  |          0 |        (?) | (?) | 1/10min |   |
 |            | Direction du vent        | °     |          0 |        360 |  45 | 1/10min |   |
 |            | Couverture nuageuse      | %     |          0 |        100 |   1 | 1/h     |   |
 |            | Indice de luminosité     | (?)   |            |            |     |         |   |
 |------------+--------------------------+-------+------------+------------+-----+---------+---|
*** Affichage
    Les données recueillies seront rendues accessible sous forme d'un
    document web public.
    
    L'écran initial présentera les données sous forme analogiques :
    - une photographie du baromètre
    - une photographie des deux thermomètres
    - une photographie de l'hygromètre
    - une rose des vents indiquant la direction du vent
    - une manche à air indiquant la force du vent
    - une une image numérisée d'un [[https://fr.wiktionary.org/wiki/udom%C3%A8tre][udomètre]] pour indiquer la hauteur
      de précipitation.
    - une photo du ciel
    - un galvanomètre présentera la luminosité

    [[file:images/exemple.jpg]]

    Au clic sur un instrument, l'écran présentera l'accès aux données
    historiques des relevés :
    - Les *données historiques de température, humidité, pression
      atmosphérique, pluviométrie, luminosité et nébulosité* seront
      consultables sous forme d'un graphe de valeurs, zoomable de
      l'heure à l'année.
    - Les *données historiques de vitesse et direction du vent* seront
      consultables sous forme d'un graphe polaire mettant en avant
      vitesse et direction, zoomable de l'heure à l'année.

 
** Exigences fonctionnelles optionnelles
   - En cas de [[http://www.meteofrance.fr/publications/glossaire?articleId=153540][rafale]], une mesure exceptionnelle pourrait être prise.
   - En cas de changement soudain de la direction du vent, une mesure exceptionnelle pourrait être faite.
   - La luminosité pourrait être exprimée en lumens.

* Calendrier prévisionnel
 | Tâche                                | Début          | Fin                 | État |
 |--------------------------------------+----------------+---------------------+------|
 | Définition des besoins               | Août 2019      | début Novembre 2019 | OK   |
 | Rédaction du calendrier prévisionnel | Novembre 2019  | Juillet 2021        | OK   |
 |--------------------------------------+----------------+---------------------+------|
 | Choix des composants                 | Septembre 2019 | fin Décembre 2019   |      |
 | Documentation composants             | Novembre 2O19  | Novembre 2019       |      |
 | Choix des fournisseurs               | Septembre 2019 | fin Décembre 2019   |      |
 | Approvisionnement                    | Septembre 2019 | fin Décembre 2019   |      |
 |--------------------------------------+----------------+---------------------+------|
 | Outils conception                    | Novembre 2O19  | Novembre 2019       |      |
 | Ouitls de programmation              | Novembre 2O19  | Novembre 2019       |      |
 |--------------------------------------+----------------+---------------------+------|
 | Prototype                            | Septembre 2019 | fin Janvier2019     |      |
 | - Girouette                          | Novembre 2019  | Novembre 2019       |      |
 | - Mesures numériques T/H/P           | Novembre 2019  | Novembre 2019       |      |
 | - Anémomètre                         | Décembre 2019  | Décembre 2019       |      |
 | - Pluviomètre                        | Janvier 2020   | Janvier 2020        |      |
 | - Photographie appareillages         | Février 2020   | Février 2020        |      |
 | - Traitement photos appareillages    | Février 2020   | Mars 2020           |      |
 |--------------------------------------+----------------+---------------------+------|
 | Tests                                | Novembre 2019  | fin Mai 2020        |      |
 | Conception amendée et précisée       | Novembre 2019  | fin Mai 2020        |      |
 |--------------------------------------+----------------+---------------------+------|
 | Fabrication                          | Octobre 2019   | fin Juin 2020       |      |
 | - Abri haut                          | Octobre 2019   | fin Novembre 2019   |      |
 | - Abris bas                          | Janvier 2020   | fin Février 2020    |      |
 | - Girouette                          | Janvier 2020   | fin Février 2020    |      |
 | - Anémomètre                         | Février 2020   | fin Mars 2020       |      |
 | - Boîtier étanche bas                | Mars 2020      | fin Avril 2020      |      |
 | - Boîtier étanche haut               | Avri 2020      | fin Mai 2020        |      |
 |--------------------------------------+----------------+---------------------+------|
 | Déploiement et tests finaux          | Juillet 2020   | fin Août 2020       |      |
 |--------------------------------------+----------------+---------------------+------|
 | Tests en situation                   | fin Août 2020  | Juillet 2021        |      |
 |--------------------------------------+----------------+---------------------+------|
 
* Composants
** Choix
*** BME 280
    Pour les mesures T/H/P numériques
*** LIS3MDL
    Pour la girouette
*** Module ESP32 DevKit V1
*** Module LoRa
    [à choisir]
*** Raspberry Pi modèle B

** Fournisseurs
   - Gotronic
   - MakerShop.de
   - Element 14
** Documentation

* Outils de conception et de programmation
  - seront utilisées mon matériel personnel ainsi que celui disponible au FabLab 
  - des demandes d'échange de matériel ou de récupération de matériel usagé ou inutilisé seront faites
  - la programmation reposera sur le cadre de développement Arduino/ESP32, le langage python du Raspberry Pi.
  - le code sera réalisé selon les préceptes de la programmation lettrée de Knuth.


