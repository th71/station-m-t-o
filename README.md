Projet Station Météo rétro-futuriste 2019-2020
====

Documentation principale
==
Pour la documentation de la construction du projet, on se reportera au fichier
**[suivi-projet.pdf](https://framagit.org/th71/station-m-t-o/blob/master/suivi_projet/suivi-projet.pdf)** du dossier *suivi-projet*.

Structure du dossier
==
- composants :: liste et informations des modules utilisés dans la réalisation
- doc :: 
 - notices techniques et essais sur les composants éléctroniques du projet
 - sources documentaires préparatoires
- fabrication :: détails et informations sur les **abris** et
  sur le **pluviomètre**
 
- src :: le code en lui-même
- suivi-projet :: documentation de suivi
- tests :: bribes de codes d'essais

 